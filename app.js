const express = require("express")
const fs = require("fs")
const uuid = require("uuid")
const app = express()
const logger = require("./loggerFun.cjs")

app.use(logger)

app.get('/html', (request, response) => {
    fs.readFile('data/data.html', 'utf-8', (err, data) => {
        if (err) {
            response.send(err.message)
        }
        else {
            response.send(data)
        }
    })
})


app.get('/json', (request, response) => {
    fs.readFile('data/data.json', 'utf-8', (err, data) => {
        if (err) {
            response.send(err.message)
        }
        else {
            response.json(JSON.parse(data))
        }
    })
})


app.get('/uuid', (request, response) => {
    const uniqId = uuid.v4();
    const uniqIdObj = {
        "uuid": uniqId,
    }
    response.send(uniqIdObj)
})


app.get('/status/:statusCode', (request, response) => {
    const code = request.params.statusCode
    response.send(`Return a response with ${code} status code`)
})


app.get('/delay/:delayTime', (request, response) => {
    const delay = request.params.delayTime
    response.setHeader('200', { "Content-Type": 'text/plain' })
    setTimeout(() => {
        response.send(`Response after ${delay} seconds`)
    }, delay * 1000)
})

app.get("/logs", (request, response) => {
    fs.readFile('data/logs.log', 'utf-8', (err, data) => {
        if (err) {
            response.send(err.message)
        }
        else {
            response.send(data)
        }
    })
})

app.listen(6000, () => {
    console.log("Server is running on port 6000")
})
